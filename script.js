let users = axios.get("https://ajax.test-danit.com/api/json/users");
let posts = axios.get("https://ajax.test-danit.com/api/json/posts");

function getUser(){
    return users;
}

function getPosts(){
    return posts;
}   

function delPost(e){
    let id = e.target.parentNode.children[2].id

    let link = new URL(`https://ajax.test-danit.com/api/json/posts/${id}`)

    fetch(link, {
    method: 'DELETE',
    })
    .then((response) => {
        if(response.ok === true){
            e.target.closest("div").style.display="none"
        } else{
            throw new Error("Error 404");
        }
    }) 
}

let cont = document.querySelector(".сontainer")

axios.all([getUser(), getPosts()])
    .then(axios.spread((user, post) =>{
        let u = user.data
        let p = post.data

        for (let i = 1; i <= u.length; i++) {
            let person = u.find((user) => i === user.id)
            let posts = p.filter((post) => post.userId === person.id)

            for (let b = 0; b < posts.length; b++) {
                console.log()
                let us = new Card(person.name, person.email,posts[b].title, posts[b].body)
                let btn = document.createElement("button")

                function getHtml(name, mail, post, txt,){
                    let div = document.createElement("div")
                    div.classList.add("card")
                    cont.append(div)

                    let n = document.createElement("p")
                    n.textContent = name 
                    n.classList.add("card__name")
                    div.append(n)

                    let m = document.createElement("p")
                    m.textContent = mail 
                    m.classList.add("card__mail")
                    div.append(m)
                    
                    let p = document.createElement("h2")
                    p.textContent = post 
                    p.classList.add("card__title")
                    div.append(p)

                    let t = document.createElement("p")
                    t.textContent = txt 
                    t.classList.add("card__txt")
                    div.append(t)

                    btn.textContent = "DELETE"
                    btn.classList.add("btn")
                    btn.addEventListener("click", delPost)
                    div.append(btn)
                }

                getHtml(us.name, us.mail, us.title, us.txt)
            }
        }

        let ps = document.querySelectorAll(".card__title")
        let psArr = Array.from(ps)

        for (let c = 0; c < psArr.length; c++) {
            const el = psArr[c];
            el.setAttribute("id", c+1)
        }
    }))

class Card{
    constructor(name,mail,title,txt,){
        this.name = name;
        this.mail = mail;
        this.title = title;
        this.txt = txt;
    }
}